package com.example.lr2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {
    @Nullable
    Double firstNumber = null;
    @Nullable
    Double secondNumber = null;
    char lastAction;
    TextView screen;
    String displayedText;

    private void display() {
        screen.setText(displayedText);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        displayedText = "";

        Button switchCalcButton = findViewById(R.id.switch_calc_button);
        screen = findViewById(R.id.screen);
        Button clear = findViewById(R.id.button_clear);
        Button backspace = findViewById(R.id.button_backspace);
        Button changeSign = findViewById(R.id.change_sign);
        Button seven = findViewById(R.id.button_7);
        Button eight = findViewById(R.id.button_8);
        Button nine = findViewById(R.id.button_9);
        Button div = findViewById(R.id.button_div);
        Button four = findViewById(R.id.button_4);
        Button five = findViewById(R.id.button_5);
        Button six = findViewById(R.id.button_6);
        Button multi = findViewById(R.id.button_multi);
        Button one = findViewById(R.id.button_1);
        Button two = findViewById(R.id.button_2);
        Button three = findViewById(R.id.button_3);
        Button minus = findViewById(R.id.button_minus);
        Button dot = findViewById(R.id.button_dot);
        Button zero = findViewById(R.id.button_0);
        Button eq = findViewById(R.id.button_eq);
        Button plus = findViewById(R.id.button_plus);

        clear.setOnClickListener(v -> {
            firstNumber = null;
            secondNumber = null;
            displayedText = "";
            display();
        });

        backspace.setOnClickListener(v -> {
            if (!displayedText.isEmpty()) {
                displayedText = displayedText.substring(0, displayedText.length() - 1);
                display();
            }
        });

        seven.setOnClickListener(v -> {
            displayedText += "7";
            display();
        });

        eight.setOnClickListener(v -> {
            displayedText += "8";
            display();
        });

        nine.setOnClickListener(v -> {
            displayedText += "9";
            display();
        });

        four.setOnClickListener(v -> {
            displayedText += "4";
            display();
        });

        five.setOnClickListener(v -> {
            displayedText += "5";
            display();
        });

        six.setOnClickListener(v -> {
            displayedText += "6";
            display();
        });

        one.setOnClickListener(v -> {
            displayedText += "1";
            display();
        });

        two.setOnClickListener(v -> {
            displayedText += "2";
            display();
        });

        three.setOnClickListener(v -> {
            displayedText += "3";
            display();
        });

        zero.setOnClickListener(v -> {
            displayedText += "0";
            display();
        });

        dot.setOnClickListener(v -> {
            if (displayedText.isEmpty()) {
                displayedText = "0.";
            } else {
                displayedText += ".";
            }
            display();
        });

        changeSign.setOnClickListener(v -> {
            if (firstNumber == null) {
                firstNumber = -1 * Double.parseDouble(displayedText);
            } else {
                secondNumber = -1 * Double.parseDouble(displayedText);
            }
            displayedText = "";
            display();
        });

        plus.setOnClickListener(v -> {
            lastAction = '+';
            if (firstNumber == null) {
                firstNumber = Double.parseDouble(displayedText);
            } else if (secondNumber == null) {
                secondNumber = Double.parseDouble(displayedText);
            }
            displayedText = "";
            display();
        });

        eq.setOnClickListener(v -> {
            if (firstNumber == null) {
                firstNumber = Double.parseDouble(displayedText);
            } else if (secondNumber == null) {
                secondNumber = Double.parseDouble(displayedText);
            }

            switch (lastAction) {
                case '+':
                    displayedText = String.valueOf(firstNumber + secondNumber);
                    break;
                case '-':
                    displayedText = String.valueOf(firstNumber - secondNumber);
                    break;
                case '*':
                    displayedText = String.valueOf(firstNumber * secondNumber);
                    break;
                case '/':
                    displayedText = String.valueOf(firstNumber / secondNumber);
                    break;
            }
            display();
            firstNumber = null;
            secondNumber = null;
        });

        minus.setOnClickListener(v -> {
            lastAction = '-';
            if (firstNumber == null) {
                firstNumber = Double.parseDouble(displayedText);
            } else if (secondNumber == null) {
                secondNumber = Double.parseDouble(displayedText);
            }
            displayedText = "";
            display();
        });

        multi.setOnClickListener(v -> {
            lastAction = '*';
            if (firstNumber == null) {
                firstNumber = Double.parseDouble(displayedText);
            } else if (secondNumber == null) {
                secondNumber = Double.parseDouble(displayedText);
            }
            displayedText = "";
            display();
        });

        div.setOnClickListener(v -> {
            lastAction = '/';
            if (firstNumber == null) {
                firstNumber = Double.parseDouble(displayedText);
            } else if (secondNumber == null) {
                secondNumber = Double.parseDouble(displayedText);
            }
            displayedText = "";
            display();
        });

        switchCalcButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, SciCalc.class);
            startActivity(intent);
        });
    }
}