package com.example.lr3;

import android.net.Uri;

public class Contact {

    private String name;
    private final String description;
    private final String email;
    private final String phone;
    private final Uri uri;

    public Contact(String name, String description, String email, String phone, Uri uri) {
        this.name = name;
        this.description = description;
        this.email = email;
        this.phone = phone;
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public Uri getUri() {
        return uri;
    }

    public String getDescription() {
        return description;
    }
}